import {UserService} from './services/UserService/user-service.service';
import {RelationshipService} from './services/RelationshipService/relationship-service.service';
import {ReactionService} from './services/ReactionService/reaction-service.service';
import {MessageService} from './services/MessageService/message-service.service';
import {AssistanceService} from './services/AssistanceService/assistance-service.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MiscolegasComponent } from './components/miscolegas/miscolegas.component';
import { InfocolegaComponent } from './components/infocolega/infocolega.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { MisdatospersonalesComponent } from './components/misdatospersonales/misdatospersonales.component';
import { MiscoleguillasperfilComponent } from './components/miscoleguillasperfil/miscoleguillasperfil.component';
import { CrearpostComponent } from './components/crearpost/crearpost.component';
import { from } from 'rxjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { InicioComponent } from './components/inicio/inicio.component';
import { EventosComponent } from './components/eventos/eventos.component';
import { ListaeventosComponent } from './components/listaeventos/listaeventos.component';
import { EventService } from './services/EventService/event-service.service';
import { MenunavComponent } from './components/menunav/menunav.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MishistorietasperfilComponent } from './components/mishistorietasperfil/mishistorietasperfil.component';

@NgModule({
  declarations: [
    AppComponent,
    MiscolegasComponent,
    InfocolegaComponent,
    PerfilComponent,
    MisdatospersonalesComponent,
    MiscoleguillasperfilComponent,
    CrearpostComponent,
    InicioComponent,
    EventosComponent,
    ListaeventosComponent,
    MenunavComponent,
    HeaderComponent,
    FooterComponent,
    MishistorietasperfilComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [EventService, AssistanceService, MessageService, ReactionService, RelationshipService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }