import { Injectable } from '@angular/core';
import { Assistance } from '../../models/assistance.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AbstractRestService } from '../abstract-rest-service.service';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class AssistanceService extends AbstractRestService<Assistance> {

  constructor(http: HttpClient) {
    super(http);
  }

  getAllEventsUserAssist(id: number){
    const url = `${environment.URL_BASE}/events/user/${id}/yesAssistance`;
    return this.http.get(url);
  }

  getAllEventsUserDesist(id: number){
    const url = `${environment.URL_BASE}/events/user/${id}/notAssistance`;
    return this.http.get(url);
  }

  assistEvent(eventId: number, userId: number): Observable<Event>{
    const url = `${environment.URL_BASE}/events/${eventId}/yesAssistance?userId=${userId}`;
    return this.http.post<Event>(url, httpOptions);
  }

  dessistEvent(eventId: number, assistanceId: number): Observable<Assistance>{
    const url = `${environment.URL_BASE}/events/${eventId}/notAssistance?assistanceId=${assistanceId}`;
    return this.http.post<Assistance>(url, httpOptions);
  }
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}