import { Injectable } from '@angular/core';
import { Event } from 'src/app/models/event.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AbstractRestService } from '../abstract-rest-service.service';
import { environment } from 'src/environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class EventService extends AbstractRestService<Event> {

  constructor(http: HttpClient) {
    super(http);
  }

  getAll(){
    const url = `${environment.URL_BASE}/events`;
    return this.http.get(url);
  }

  getOne(id: number): Observable<Event>{
    const url = `${environment.URL_BASE}/events/${id}`;
    return this.http.get<Event>(url);
  }

  add(event: Event): Observable<Event>{
    const url = `${environment.URL_BASE}/events`;
    return this.http.post<Event>(url, event, httpOptions);
  }
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};