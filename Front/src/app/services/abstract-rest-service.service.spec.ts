import { TestBed } from '@angular/core/testing';

import { AbstractRestService } from './abstract-rest-service.service';

describe('AbstractRestServiceService', () => {
  let service: AbstractRestService<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AbstractRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
