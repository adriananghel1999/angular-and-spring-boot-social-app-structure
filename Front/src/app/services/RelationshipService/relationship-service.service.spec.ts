import { TestBed } from '@angular/core/testing';

import { RelationshipServiceService } from './relationship-service.service';

describe('RelationshipServiceService', () => {
  let service: RelationshipServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RelationshipServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
