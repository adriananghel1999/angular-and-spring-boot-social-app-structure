import { Injectable } from '@angular/core';
import { Relationship } from 'src/app/models/relationship.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AbstractRestService } from '../abstract-rest-service.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class RelationshipService extends AbstractRestService<Relationship> {

  constructor(http: HttpClient) {
    super(http);
  }

  getFriendsList(id: number): Observable<Relationship[]>{
    const url = `${environment.URL_BASE}/users/${id}/friendsRelationship`;
    return this.http.get<Relationship[]>(url);
  }

  deleteRelationship(id: number): Observable<{}>{
    const url = `${environment.URL_BASE}/users/relationship/${id}`;
    return this.http.delete<{}>(url);
  }

  getPendingList(id: number): Observable<Relationship[]>{
    const url = `${environment.URL_BASE}/users/${id}/pendingRelationship`;
    return this.http.get<Relationship[]>(url);
  }

  updateRelationship(id: number){
    const url = `${environment.URL_BASE}/users/relationship/${id}`;
    return this.http.put(url, httpOptions);
  }

}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}