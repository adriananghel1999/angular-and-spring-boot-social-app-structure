import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export abstract class AbstractRestService<T> {

  constructor(protected http: HttpClient) { }
/*
  getAll(){
    return this.http.get(environment.URL_BASE);
  }

  getOne(id: number): Observable<T>{
    const url = `${environment.URL_BASE}/${id}`;
    return this.http.get<T>(url);
  }

  add(item: T): Observable<T>{
    return this.http.post<T>(environment.URL_BASE, item, httpOptions);
  }

  update(item: T, id: number): Observable<T>{
    const url = `${environment.URL_BASE}/${id}`;
    return this.http.put<T>(url, item, httpOptions);
  }

  delete(id: number): Observable<{}>{
    const url = `${environment.URL_BASE}/${id}`;
    return this.http.delete(url, httpOptions);
  }*/
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};