import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractRestService } from '../abstract-rest-service.service';
import { Reaction } from 'src/app/models/reaction.model';

@Injectable()
export class ReactionService extends AbstractRestService<Reaction> {

  constructor(http: HttpClient) {
    super(http);
  }
}
