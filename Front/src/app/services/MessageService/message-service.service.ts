import { Injectable } from '@angular/core';
import { Message } from 'src/app/models/message.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AbstractRestService } from '../abstract-rest-service.service';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Reaction } from 'src/app/models/reaction.model';
import { ReactionType } from 'src/app/enums/reactionType.enum';

@Injectable()
export class MessageService extends AbstractRestService<Message> {

  constructor(http: HttpClient) {
    super(http);
  }

  getAll(): Observable<Message[]>{
    const url = `${environment.URL_BASE}/messages`;
    return this.http.get<Message[]>(url);
  }

  getOne(id: number): Observable<Message>{
    const url = `${environment.URL_BASE}/messages/${id}`;
    return this.http.get<Message>(url);
  }

  add(message: Message, userId: number): Observable<Message>{
    const url = `${environment.URL_BASE}/messages?userId=${userId}`;
    return this.http.post<Message>(url, message, httpOptions);
  }

  getAllMessagesUser(id: number): Observable<Object> {
    const url = `${environment.URL_BASE}/messages/user/${id}`;
    return this.http.get(url);
    }

    getMessageByUserId(id: number): Observable<Message[]> {
      const url = `${environment.URL_BASE}/messages/user/${id}`;
      return this.http.get<Message[]>(url);
    }

    addReaction(reaction: Reaction, messageId: number, userId: number): Observable<Reaction> {
      const url = `${environment.URL_BASE}/messages/${messageId}/reactions?userId=${userId}`;
      return this.http.post<Reaction>(url, reaction, httpOptions);
    }
  
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};