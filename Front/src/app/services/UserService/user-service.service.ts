import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AbstractRestService } from '../abstract-rest-service.service';
import { User } from 'src/app/models/user.model';
import { environment } from 'src/environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class UserService extends AbstractRestService<User> {
 
  constructor(http: HttpClient) {
    super(http);
  }

  getAll(){
    const url = `${environment.URL_BASE}/users`;
    return this.http.get(url);
  }


  getOne(id: number): Observable<User>{
    const url = `${environment.URL_BASE}/users/${id}`;
    return this.http.get<User>(url);
  }

  getOneFull(id: number): Observable<User>{
    const url = `${environment.URL_BASE}/users/fullUser/${id}`;
    return this.http.get<User>(url);
  }

  updateUser(id: number, user:User): Observable<User>{
    const url = `${environment.URL_BASE}/users/${id}`;
    return this.http.put<User>(url, user);
  }

  inviteFriend(id: number, userName:String){
    const url = `${environment.URL_BASE}/users/${id}/inviteFriend?user=${userName}`;
    return this.http.post(url, httpOptions);
  }

};

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}