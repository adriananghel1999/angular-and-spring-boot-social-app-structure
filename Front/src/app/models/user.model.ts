import { Message } from "./message.model";
import { Event } from './event.model';
import { Reaction } from "./reaction.model";
import { Assistance } from "./assistance.model";
import { Relationship } from "./relationship.model";

export class User {
    id:number;
    name:string;
    surname:string;
    birthDate:Date;
    startDate:Date;
    user:string;
    pass:string;
    message:Message[];
    event:Event[];
    reaction:Reaction[];
    assistance:Assistance[];
    relationship1:Relationship[];
    relationship2:Relationship[];
}
