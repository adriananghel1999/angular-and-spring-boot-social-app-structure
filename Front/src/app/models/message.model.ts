import { User } from "./user.model";
import { Reaction } from "./reaction.model";

export class Message {
    id:number;
    content:string;
    publishDate:Date;
    user: User;
}
