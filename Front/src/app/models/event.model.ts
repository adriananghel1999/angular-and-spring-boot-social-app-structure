import { User } from './user.model';
import { Assistance } from './assistance.model';

export class Event {
    id: number;
    name: string;
    description: string;
    eventDate: Date;
    user: User;
    assistance:Assistance[];
}
