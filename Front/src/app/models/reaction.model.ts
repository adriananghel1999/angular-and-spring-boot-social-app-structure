import { ReactionType } from "../enums/reactionType.enum";
import { User } from "./user.model";
import { Message } from "./message.model";

export class Reaction {
    id:number;
    reactionType:ReactionType;
    user:User;
    message:Message;

}
