import { State } from '../enums/state.enum';
import { User } from './user.model';
import { Event } from './event.model';

export class Assistance {
    id: number;
    state: State;
    user: User;
    event: Event;
}