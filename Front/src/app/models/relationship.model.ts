import { RelationshipEnum } from "../enums/relationshipEnum.enum";
import { User } from "./user.model";

export class Relationship {
    id:number;
    state:RelationshipEnum;
    userFollowing:User;
    userFollow:User;
}
