import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MiscolegasComponent } from './components/miscolegas/miscolegas.component'
import {InicioComponent} from './components/inicio/inicio.component'
import {EventosComponent} from './components/eventos/eventos.component'
import {PerfilComponent} from './components/perfil/perfil.component'

const routes: Routes = [
  {path:'', component: InicioComponent},
  {path: 'miscolegas', component: MiscolegasComponent },
  {path: 'inicio', component: InicioComponent },
  {path: 'eventos', component: EventosComponent},
  {path: 'perfil', component: PerfilComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


