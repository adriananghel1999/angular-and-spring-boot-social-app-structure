import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/UserService/user-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  constructor(private userService: UserService) { }
  user: User = new User();

  ngOnInit() {

    this.userService.getOne(1)
      .subscribe((data: User) => this.user = data['User'],
        error => console.error(error),
        () => console.log("userId is loaded")
      );
  }

}
