import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {UserService} from '../../services/UserService/user-service.service'
import { User } from 'src/app/models/user.model';
import { MessageService } from 'src/app/services/MessageService/message-service.service';
import { Message } from 'src/app/models/message.model';
import { FormGroup, FormControl } from '@angular/forms';
import { ReactionType } from '../../enums/reactionType.enum'
import {Reaction} from '../../models/reaction.model'

@Component({
  selector: 'app-crearpost',
  templateUrl: './crearpost.component.html',
  styleUrls: ['./crearpost.component.css']
})
export class CrearpostComponent implements OnInit {

  messages: Message[] = [];
  message = new Message();
  user:User = new User();
  ngForm:FormGroup;

 reaction: Reaction = new Reaction();
  loveReaction = ReactionType.LOVE;
  likeReaction = ReactionType.LIKE;
  hateReaction = ReactionType.HATE;


  constructor(private messageService:MessageService, private userService: UserService) { 

    this.ngForm = new FormGroup({
      content: new FormControl('content'),
    }
    );  
  }

  ngOnInit(){
    
    this.userService.getOne(1)
      .subscribe((data: User) => this.user = data['User'],
        error => console.error(error),
        () => console.log("userId is loaded")
      );
  
      this.messageService.getAll().subscribe(
        (data: Message[]) => (this.messages = data),
        (error) => console.error(error),
        () => console.log(this.messages)
      );

  }

  createNewMessagge(message: Message){
    message.publishDate = new Date();
    this.messageService.add(message, this.user.id)
    .subscribe(message => this.messages.push(message['messageDto']),
    () => console.log(this.message)
    );
    this.ngForm.reset();
  }

  addReaction(reactionType: ReactionType, messageId: number) {
    this.reaction.reactionType = reactionType;
    this.messageService.addReaction(this.reaction, messageId, this.user.id).subscribe(
    () => console.log("+1 en la reacion " + this.reaction.reactionType),
    (error) => (console.error(error)),
    () => (console.log('Se ha añadido la reacción correctamente'))
    );
  }
}