import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/UserService/user-service.service';
import { RelationshipEnum } from 'src/app/enums/relationshipEnum.enum';
import { Relationship } from 'src/app/models/relationship.model';

@Component({
  selector: 'app-infocolega',
  templateUrl: './infocolega.component.html',
  styleUrls: ['./infocolega.component.css']
})
export class InfocolegaComponent implements OnInit {

  colegaSelected: boolean = false;
  desconocidoSelected: boolean = false;

  userIdSession: number = 1;
  user: User = new User();
  colega: User = new User();
  desconocido: User = new User();

  @Input()
  relationFromMisColegas: Relationship = new Relationship();

  @Input()
  userFromMisColegas: User = new User();

  @Input()
  viewSelectedFromMisColegas: string;

  userFromMisColegasFull: User = new User();

  constructor(private userService: UserService) { }

  ngOnInit(): void {

    this.userService.getOneFull(this.userIdSession).subscribe(
      (data: User) => (this.user = data['User'])
    );

  }

  ngOnChanges() {

    let userFromMisColegas: User = new User();

    if (this.viewSelectedFromMisColegas === "relation") {

      if (this.relationFromMisColegas.userFollow.id === this.user.id) {
        userFromMisColegas = this.relationFromMisColegas.userFollowing;
      } else if (this.relationFromMisColegas.userFollowing.id == this.user.id) {
        userFromMisColegas = this.relationFromMisColegas.userFollow;
      }

    } else if (this.viewSelectedFromMisColegas === "user") {
      userFromMisColegas = this.userFromMisColegas;

    }

    this.userService.getOneFull(userFromMisColegas.id).subscribe(
      (data: User) => {
        (this.userFromMisColegasFull = data['User']);

        if (this.isUserFriend(this.userFromMisColegasFull)) {
          this.colega = this.userFromMisColegasFull;
          this.colegaSelected = true;
          this.desconocidoSelected = false;
        } else {
          this.desconocido = this.userFromMisColegasFull;
          this.colegaSelected = false;
          this.desconocidoSelected = true;
        }

      }
    );

  }

  isUserFriend(user: User): boolean {
    let result: boolean = false;

    for (let relations of this.user.relationship1) {
      if (user.id === relations.userFollow.id && relations.state === RelationshipEnum.FRIEND) {
        result = true;
      }

      if (user.id === relations.userFollowing.id && relations.state === RelationshipEnum.FRIEND) {
        result = true;
      }
    }

    for (let relations of this.user.relationship2) {
      if (user.id === relations.userFollow.id && relations.state === RelationshipEnum.FRIEND) {
        result = true;
      }

      if (user.id === relations.userFollowing.id && relations.state === RelationshipEnum.FRIEND) {
        result = true;
      }
    }

    return result;
  }

  isUserPending(user: User): boolean {
    let result: boolean = false;

    for (let relations of this.user.relationship1) {
      if (user.id === relations.userFollow.id && relations.state === RelationshipEnum.PENDING) {
        result = true;
      }

      if (user.id === relations.userFollowing.id && relations.state === RelationshipEnum.PENDING) {
        result = true;
      }
    }

    for (let relations of this.user.relationship2) {
      if (user.id === relations.userFollow.id && relations.state === RelationshipEnum.PENDING) {
        result = true;
      }

      if (user.id === relations.userFollowing.id && relations.state === RelationshipEnum.PENDING) {
        result = true;
      }
    }

    return result;
  }

  sendFriendRequest(user: User) {
    this.userService.inviteFriend(this.user.id, user.user).subscribe();
  }

  getRelationshipNameSurname(relation: Relationship) {
    if (relation.userFollow.id === this.user.id) {
      return relation.userFollowing.name + " " + relation.userFollowing.surname;
    } else if (relation.userFollowing.id == this.user.id) {
      return relation.userFollow.name + " " + relation.userFollow.surname;
    }
  }

}
