import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfocolegaComponent } from './infocolega.component';

describe('InfocolegaComponent', () => {
  let component: InfocolegaComponent;
  let fixture: ComponentFixture<InfocolegaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfocolegaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfocolegaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
