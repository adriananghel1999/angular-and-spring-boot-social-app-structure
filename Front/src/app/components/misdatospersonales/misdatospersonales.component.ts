import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/UserService/user-service.service';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { error } from '@angular/compiler/src/util';
import { asapScheduler } from 'rxjs';
import { disableDebugTools } from '@angular/platform-browser';
import { DepFlags } from '@angular/compiler/src/core';


@Component({
  selector: 'app-misdatospersonales',
  templateUrl: './misdatospersonales.component.html',
  styleUrls: ['./misdatospersonales.component.css']
})
export class MisdatospersonalesComponent implements OnInit {

  user = new User();

  update:boolean = true;
  
  constructor(private userService:UserService) {
    
   }

  ngOnInit(): void {
    
    this.userService.getOne(1).subscribe(
      (data:User) => (this.user = data['User']), 
      (error) => console.error(error),
      () => console.log(this.user)
    );

  }

  switchUpdate() {
    if (this.update) {
      this.update = false;     
    } else if(this.update == false) {
      this.update = true;
    }
  }

 datosUpdate(){
   this.userService.updateUser(this.user.id, this.user).subscribe();
   this.switchUpdate();

 }

}