import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisdatospersonalesComponent } from './misdatospersonales.component';

describe('MisdatospersonalesComponent', () => {
  let component: MisdatospersonalesComponent;
  let fixture: ComponentFixture<MisdatospersonalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisdatospersonalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisdatospersonalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
