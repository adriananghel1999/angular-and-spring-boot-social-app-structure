import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiscoleguillasperfilComponent } from './miscoleguillasperfil.component';

describe('MiscoleguillasperfilComponent', () => {
  let component: MiscoleguillasperfilComponent;
  let fixture: ComponentFixture<MiscoleguillasperfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiscoleguillasperfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiscoleguillasperfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
