import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Relationship } from 'src/app/models/relationship.model';
import { RelationshipService } from 'src/app/services/RelationshipService/relationship-service.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/UserService/user-service.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-miscoleguillasperfil',
  templateUrl: './miscoleguillasperfil.component.html',
  styleUrls: ['./miscoleguillasperfil.component.css']
})
export class MiscoleguillasperfilComponent implements OnInit {

  idUserSession: number = 1;
  user: User = new User();

  currentRoute: string = "";

  relationships: Relationship[] = [];
  pendingRelationships: Relationship[] = [];

  searchRelationships: Relationship[] = [];
  searchColeguillas: User[] = [];

  allUsers: User[] = [];

  relationSelected: Relationship = new Relationship();
  userSelected: User = new User();
  viewSelected:string;

  @Output() someEvent = new EventEmitter<string>();

  @Input()
  stringSearch: string;


  constructor(private relationshipService: RelationshipService, private userService: UserService, private router: Router) {

  }

  ngOnInit(): void {

    if (this.router.url === "/miscolegas") {
      this.currentRoute = "/miscolegas";
    } else if (this.router.url === "/perfil") {
      this.currentRoute = "/perfil";
    }

    this.relationshipService.getFriendsList(this.idUserSession).subscribe(
      (data: Relationship[]) => {
        (this.relationships = data['Friends']);
        (error) => console.error(error);
        this.loadStrangers();
      }
    );

    this.relationshipService.getPendingList(this.idUserSession).subscribe(
      (data: Relationship[]) => {
        (this.pendingRelationships = data['Pendings']);
        (error) => console.error(error);
      }
    );

    this.userService.getOne(this.idUserSession)
      .subscribe((data: User) => this.user = data['User']
      );

  }

  loadStrangers() {
    if (this.currentRoute === "/miscolegas") {
      this.userService.getAll().subscribe(
        (data: User[]) => {
          this.allUsers = data;
          // quito duplicados de relationship en allusers
          for (let relation of this.relationships) {
            if (this.allUsers.find(user => user.id === relation.userFollow.id)) {
              this.allUsers = this.allUsers.filter(user => user.id !== relation.userFollow.id);
            }

            if (this.allUsers.find(user => user.id === relation.userFollowing.id)) {
              this.allUsers = this.allUsers.filter(user => user.id !== relation.userFollowing.id);
            }

          }
          //hardcode de quitar al usuario que a iniciado sesion que no se muestre en la lista

          this.allUsers = this.allUsers.filter(user => user.id !== this.user.id);
        }
      );
    }
  }



  searchColeguilla(): boolean {

    let searchRelationships: Relationship[] = [];
    let searchColeguillas: User[] = [];

    for (let relation of this.relationships) {
      if (relation.userFollow.name.toLowerCase().includes(this.stringSearch.toLowerCase()) || relation.userFollow.surname.toLowerCase().includes(this.stringSearch.toLowerCase())) {
        searchRelationships.push(relation);
      }
    }

    for (let user of this.allUsers) {
      if (user.name.toLowerCase().includes(this.stringSearch.toLowerCase()) || user.surname.toLowerCase().includes(this.stringSearch.toLowerCase())) {
        searchColeguillas.push(user);
      }
    }

    this.searchRelationships = searchRelationships;
    this.searchColeguillas = searchColeguillas;

    if (this.stringSearch) {
      return true;
    }
    return false;
  }

  deleteFriendship(relation: Relationship) {
    this.relationshipService.deleteRelationship(relation.id).subscribe(
      () => {
        this.relationships = this.relationships.filter((r) => {
          return r.id !== relation.id;
        });
        this.loadStrangers();
      }
    );

    
  }

  updateFriendship(relation: Relationship){
    this.relationshipService.updateRelationship(relation.id).subscribe(
      () => {
        this.pendingRelationships = this.pendingRelationships.filter((r) => {
          return r.id !== relation.id;
        });

        this.relationshipService.getFriendsList(this.idUserSession).subscribe(
          (data: Relationship[]) => {
            (this.relationships = data['Friends']);
            (error) => console.error(error);
          }
        );
      }
    );
  }

  selectRelation(relation:Relationship){
    this.viewSelected = "relation";
    this.someEvent.next("selectedViewChild");
    this.relationSelected = relation;
    this.someEvent.next("relationViewChild");
  }

  selectUser(user:User){
    this.viewSelected = "user";
    this.someEvent.next("selectedViewChild");
    this.userSelected = user;
    this.someEvent.next("userViewChild");
  }

  getRelationshipNameSurname(relation:Relationship){
    if(relation.userFollow.id === this.user.id){
      return relation.userFollowing.name + " " + relation.userFollowing.surname;
    } else if (relation.userFollowing.id == this.user.id){
      return relation.userFollow.name + " " + relation.userFollow.surname;
    }
  }

}
