import { EventService } from '../../services/EventService/event-service.service';
import { Component, OnInit } from '@angular/core';
import { Event } from '../../models/event.model';
import { AssistanceService } from 'src/app/services/AssistanceService/assistance-service.service';
import { UserService } from 'src/app/services/UserService/user-service.service';
import { User } from 'src/app/models/user.model';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-listaeventos',
  templateUrl: './listaeventos.component.html',
  styleUrls: ['./listaeventos.component.css']
})
export class ListaeventosComponent implements OnInit {

  allEvents: Event[] = [];
  allEventsUserAssists: Event[] = [];
  allEventsUserDessist: Event[] = [];

  searchEvents: Event[] = [];
  searchText: string;
  
  user:User = new User();
  newEvent:Event = new Event();

  ngForm:FormGroup;

  idUserSession:number = 1;

  constructor(private eventService: EventService, private assistanceService: AssistanceService, private userService: UserService) {
    this.ngForm = new FormGroup({
      tittle: new FormControl('tittle'),
      description: new FormControl('description'),
      date: new FormControl('date')
    }
    );  
   }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(){
    this.userService.getOneFull(this.idUserSession)
      .subscribe((data: User) => this.user = data['User'],
        error => console.error(error),
        () => console.log("userId is loaded")
      );

    this.eventService.getAll()
      .subscribe((data: Event[]) => this.allEvents = data,
        error => console.error(error),
        () => console.log("My events list is loaded")
      );

    this.assistanceService.getAllEventsUserAssist(this.idUserSession).
      subscribe((data: Event[]) => this.allEventsUserAssists = data,
        error => console.error(error),
        () => console.log("My assist events list is loaded")
      );

    this.assistanceService.getAllEventsUserDesist(this.idUserSession).
      subscribe((data: Event[]) => this.allEventsUserDessist = data,
        error => console.error(error),
        () => console.log("My dessist events list is loaded")
      );
  }

  searchEvent() {
    let searchEvents: Event[] = [];

    for (let evento of this.allEvents) {

      if (evento.name.toLowerCase().includes(this.searchText.toLowerCase())) {
        searchEvents.push(evento);
      } else if (evento.description.toLowerCase().includes(this.searchText.toLowerCase())) {
        searchEvents.push(evento);
      } else if (evento.user.name.toLowerCase().includes(this.searchText.toLowerCase()) || evento.user.surname.toLowerCase().includes(this.searchText.toLowerCase()) || evento.user.user.toLowerCase().includes(this.searchText.toLowerCase())) {
        searchEvents.push(evento);
      } else if (evento.eventDate.toString().toLowerCase().includes(this.searchText.toLowerCase())) {
        searchEvents.push(evento);
      }

    }

    this.searchEvents = searchEvents;

  }

  assistEvent(eventId: number) {
    this.assistanceService.assistEvent(eventId, this.idUserSession).subscribe(a => this.loadData());
    
  }

  dessistEvent(eventId: number) {
    let assistanceId: number;

    for (let assistance of this.user.assistance) {
      if (assistance.event.id === eventId) {
        assistanceId = assistance.id;
      }
    }

    this.assistanceService.dessistEvent(eventId, assistanceId).subscribe(a => this.loadData());
  }

  checkIfUserAssists(event: Event): boolean {
    let result: boolean = false;

    for (let assistingEvent of this.allEventsUserAssists) {
      if (assistingEvent.id === event.id) {
        result = true;
      }

    }

    return result;
    
  }

  checkIfUserIsCreator(event: Event): boolean{
    let result: boolean = false;

    if(event.user.id === this.user.id){
      result = true;
    }

    return result;
  }

  createNewEvent(event: Event){
    this.newEvent.user = this.user;
    this.eventService.add(event)
    .subscribe(event => this.allEvents.push(event['newEvent']));
    this.ngForm.reset();
  }

}
