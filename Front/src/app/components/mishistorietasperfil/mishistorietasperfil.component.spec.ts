import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MishistorietasperfilComponent } from './mishistorietasperfil.component';

describe('MishistorietasperfilComponent', () => {
  let component: MishistorietasperfilComponent;
  let fixture: ComponentFixture<MishistorietasperfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MishistorietasperfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MishistorietasperfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
