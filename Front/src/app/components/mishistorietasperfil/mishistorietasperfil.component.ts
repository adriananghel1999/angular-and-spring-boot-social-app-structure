import { Component, OnInit } from '@angular/core';
import { Message } from 'src/app/models/message.model';
import { MessageService } from 'src/app/services/MessageService/message-service.service';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/UserService/user-service.service';

@Component({
  selector: 'app-mishistorietasperfil',
  templateUrl: './mishistorietasperfil.component.html',
  styleUrls: ['./mishistorietasperfil.component.css']
})
export class MishistorietasperfilComponent implements OnInit {

  messages: Message[];
  user:User;

  constructor(
    private messageService:MessageService,
    private userService:UserService
    ) {}

  ngOnInit(): void {
   this.messageService.getMessageByUserId(1).subscribe(
     (data:Message[]) => (this.messages = data),
     (error) => console.error(error),
     () => console.log(this.messages)
   );
   this.userService.getOne(1).subscribe(
    (data:User) => (this.user = data['User']), 
    (error) => console.error(error),
    () => console.log(this.user)
  );

  }

}
