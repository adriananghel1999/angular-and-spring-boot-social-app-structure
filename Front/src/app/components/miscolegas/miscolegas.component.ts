import { Component, OnInit, ViewChild } from '@angular/core';
import { MiscoleguillasperfilComponent } from '../miscoleguillasperfil/miscoleguillasperfil.component';
import { User } from 'src/app/models/user.model';
import { Relationship } from 'src/app/models/relationship.model';

@Component({
  selector: 'app-miscolegas',
  templateUrl: './miscolegas.component.html',
  styleUrls: ['./miscolegas.component.css']
})
export class MiscolegasComponent implements OnInit {

  searchText:string = "";

  @ViewChild(MiscoleguillasperfilComponent, {static:false}) hijo:MiscoleguillasperfilComponent;

  viewChild:Relationship;
  viewUser:User;
  viewSelected:string = "";

  constructor() { }

  ngOnInit(): void {
  }

  relationViewChild(){
    this.viewChild=this.hijo.relationSelected;
  }

  userViewChild(){
    this.viewUser=this.hijo.userSelected;
  }

  selectedViewChild(){
    this.viewSelected=this.hijo.viewSelected;
  }

}
