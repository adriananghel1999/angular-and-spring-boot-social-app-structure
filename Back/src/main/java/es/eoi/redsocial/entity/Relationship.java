package es.eoi.redsocial.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.eoi.redsocial.enums.RelationshipEnum;

@Entity
@Table(name = "relationship")
public class Relationship {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private RelationshipEnum state;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_userFollowing", referencedColumnName = "ID")
	private User userFollowing;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_userFollow", referencedColumnName = "ID")
	private User userFollow;

	public RelationshipEnum getState() {
		return state;
	}

	public void setState(RelationshipEnum state) {
		this.state = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUserFollowing() {
		return userFollowing;
	}

	public void setUserFollowing(User userFollowing) {
		this.userFollowing = userFollowing;
	}

	public User getUserFollow() {
		return userFollow;
	}

	public void setUserFollow(User userFollow) {
		this.userFollow = userFollow;
	}

	public Relationship(Long id, RelationshipEnum state, User userFollowing, User userFollow) {
		super();
		this.id = id;
		this.state = state;
		this.userFollowing = userFollowing;
		this.userFollow = userFollow;
	}

	public Relationship() {
		super();
		// TODO Auto-generated constructor stub
	}

}
