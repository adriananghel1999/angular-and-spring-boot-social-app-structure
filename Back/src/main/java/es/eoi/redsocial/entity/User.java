package es.eoi.redsocial.entity;


import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "user")
public class User {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String name;

	@Column
	private String surname;

	@Column
	private Date birthDate;

	@Column(updatable = false)
	@CreationTimestamp
	private Date startDate;

	@Column(unique = true)
	private String user;

	@Column
	private String pass;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<Message> message;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<Event> event;

	@OneToMany(mappedBy = "user")
	private List<Reaction> reaction;

	@OneToMany(mappedBy = "user")
	private List<Assistance> assistance;

	@OneToMany(mappedBy = "userFollowing", fetch = FetchType.LAZY)
	private List<Relationship> relationship1;

	@OneToMany(mappedBy = "userFollow", fetch = FetchType.LAZY)
	private List<Relationship> relationship2;

	


	public User(Long id, String name, String surname, Date birthDate, Date startDate, String user, String pass,
			List<Message> message, List<Event> event, List<Reaction> reaction, List<Assistance> assistance,
			List<Relationship> relationship1, List<Relationship> relationship2) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.birthDate = birthDate;
		this.startDate = startDate;
		this.user = user;
		this.pass = pass;
		this.message = message;
		this.event = event;
		this.reaction = reaction;
		this.assistance = assistance;
		this.relationship1 = relationship1;
		this.relationship2 = relationship2;
	}
	
	

	public User() {
		super();
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public List<Message> getMessage() {
		return message;
	}

	public void setMessage(List<Message> message) {
		this.message = message;
	}

	public List<Event> getEvent() {
		return event;
	}

	public void setEvent(List<Event> event) {
		this.event = event;
	}

	public List<Reaction> getReaction() {
		return reaction;
	}

	public void setReaction(List<Reaction> reaction) {
		this.reaction = reaction;
	}

	public List<Assistance> getAssistance() {
		return assistance;
	}

	public void setAssistance(List<Assistance> assistance) {
		this.assistance = assistance;
	}

	public List<Relationship> getRelationship1() {
		return relationship1;
	}

	public void setRelationship1(List<Relationship> relationship1) {
		this.relationship1 = relationship1;
	}

	public List<Relationship> getRelationship2() {
		return relationship2;
	}

	public void setRelationship2(List<Relationship> relationship2) {
		this.relationship2 = relationship2;
	}


}
