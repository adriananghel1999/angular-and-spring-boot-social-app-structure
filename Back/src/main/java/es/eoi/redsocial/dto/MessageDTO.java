package es.eoi.redsocial.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import es.eoi.redsocial.entity.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageDTO {

    private Long id;

    private String content;
    
    private UserDTO user;
    
    private Date publishDate;

     
}