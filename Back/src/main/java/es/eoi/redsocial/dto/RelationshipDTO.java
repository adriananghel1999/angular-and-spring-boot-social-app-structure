package es.eoi.redsocial.dto;

import es.eoi.redsocial.enums.RelationshipEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RelationshipDTO {

	private Long id;

	private RelationshipEnum state;

	private UserDTO userFollowing;
	
	private UserDTO userFollow;

}
