package es.eoi.redsocial.dto;

import es.eoi.redsocial.enums.ReactionType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReactionDTO {
	private Long id;
	private ReactionType reactionType;

}
