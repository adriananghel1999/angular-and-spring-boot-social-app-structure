
package es.eoi.redsocial.dto;

import es.eoi.redsocial.enums.State;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AssistanceDTO {
	private Long id;
	private State state;
	private EventDTO event;
	private UserDTO user;
}