package es.eoi.redsocial.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserAllDTO {
	
	private Long id;

	private String name;

	private String surname;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date birthDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date startDate;

	private String user;

	private String pass;
	
	private List<AssistanceDTO> assistance;
	
	private List<EventDTO> event;
	
	private List<ReactionDTO> reaction;
		
	private List<RelationshipDTO> relationship1;
	
	private List<RelationshipDTO> relationship2;

	private List<MessageDTO> message;

	
}