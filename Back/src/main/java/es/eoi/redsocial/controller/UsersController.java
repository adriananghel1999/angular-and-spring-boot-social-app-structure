package es.eoi.redsocial.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.eoi.redsocial.dto.RelationshipDTO;
import es.eoi.redsocial.dto.UserAllDTO;
import es.eoi.redsocial.dto.UserDTO;
import es.eoi.redsocial.entity.Relationship;
import es.eoi.redsocial.entity.User;
import es.eoi.redsocial.enums.RelationshipEnum;
import es.eoi.redsocial.service.RelationshipService;
import es.eoi.redsocial.service.UserService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private UserService service;

	@Autowired
	private RelationshipService relationshipService;

	private final ModelMapper modelMapper = new ModelMapper();

	@GetMapping
	public ResponseEntity<?> findAllUsers() {
		Map<String, Object> response = new HashMap<String, Object>();

		List<UserDTO> userDto = new ArrayList<UserDTO>();

		java.lang.reflect.Type type = new TypeToken<List<UserDTO>>() {
		}.getType();
		try {

			userDto = modelMapper.map(service.findAllUser(), type);
		} catch (DataAccessException e) {
			response.put("message", "Error acessing to database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

		}

		return new ResponseEntity<>(userDto, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findUserById(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<String, Object>();
		User user;

		try {

			user = service.finByIdUser(id);

		} catch (NoSuchElementException e) {
			response.put("message", "User with ID ".concat(id.toString().concat(" don't exist.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);

		}

		UserDTO userDto = modelMapper.map(user, UserDTO.class);
		response.put("User", userDto);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@GetMapping("/fullUser/{id}")
	public ResponseEntity<?> findFullUserId(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<String, Object>();
		response.clear();
		User user;
		UserAllDTO userDto;
		try {
			user = service.finByIdUser(id);
		} catch (DataAccessException exception) {
			response.put("message", "Error al acceder a la base de datos");
			response.put("error",
					exception.getMessage().concat(": ").concat(exception.getMostSpecificCause().getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		if (user == null) {
			response.put("message", "El Ususario con ID ".concat(id.toString().concat(" no existe")));
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		userDto = modelMapper.map(user, UserAllDTO.class);
		response.put("User", userDto);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

	}

	@PostMapping
	public ResponseEntity<?> createUser(@RequestBody UserDTO userDto) {
		Map<String, Object> response = new HashMap<String, Object>();
		User user = modelMapper.map(userDto, User.class);
		User newUser = null;
		try {
			newUser = service.saveUser(user);
		} catch (DataAccessException e) {
			response.put("message", "Error inserting into the database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("newUser", modelMapper.map(newUser, UserDTO.class));
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateUser(@RequestBody UserDTO userDTO, @PathVariable Long id) {
		Map<String, Object> response = new HashMap<String, Object>();
		User currentUser;
		try {
			currentUser = service.finByIdUser(id);
		} catch (NoSuchElementException e) {
			response.put("message", "The User with id '".concat(id.toString().concat("' not exist.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		currentUser = modelMapper.map(userDTO, User.class);
		service.saveUser(currentUser);
		response.put("message", "user successfully modified");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

	}

	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestParam String user, @RequestParam String pass) {
		Map<String, Object> response = new HashMap<String, Object>();
		User userLogin = service.findUserAndPass(user, pass);
		UserDTO userDto = new UserDTO();

		try {

			if (userLogin == null) {
				return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);

			}

		} catch (DataAccessException e) {
			response.put("message", "Username or password incorrect");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

		}

		userDto = modelMapper.map(userLogin, UserDTO.class);
		return new ResponseEntity<UserDTO>(userDto, HttpStatus.OK);
	}

	@GetMapping("/{id}/friendsRelationship")
	public ResponseEntity<?> getFriendsList(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<String, Object>();
		List<RelationshipDTO> friendsList;

		try {

			friendsList = relationshipService.getAllRelationships().stream()
					.filter((r) -> (r.getUserFollow().getId() == id || r.getUserFollowing().getId() == id)
							&& r.getState().equals(RelationshipEnum.FRIEND))
					.map(r -> modelMapper.map(r, RelationshipDTO.class)).collect(Collectors.toList());

		} catch (DataAccessException e) {
			response.put("message", "This user has no friends on their list.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

		}

		response.put("Friends", friendsList);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@GetMapping("/{id}/pendingRelationship")
	public ResponseEntity<Map<String, Object>> getPendingList(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<String, Object>();
		List<RelationshipDTO> pendingsList = relationshipService.getAllRelationships().stream()
				.filter((r) -> r.getUserFollow().getId() == id && r.getState().equals(RelationshipEnum.PENDING))
				.map(r -> modelMapper.map(r, RelationshipDTO.class)).collect(Collectors.toList());
		response.put("Pendings", pendingsList);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PostMapping("/{id}/inviteFriend")
	public ResponseEntity<Map<String, Object>> inviteFriend(@PathVariable Long id, @RequestParam("user") String user) {
		Map<String, Object> response = new HashMap<String, Object>();
		User hostUser = service.finByIdUser(id);
		User guestUser = service.findUserUsers(user);

		Relationship newRelationship = new Relationship();
		try {
			newRelationship = relationshipService.saveRelationship(hostUser, guestUser);
		} catch (IllegalArgumentException e) {
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		response.put("newRelationship", modelMapper.map(newRelationship, RelationshipDTO.class));
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@PutMapping("/relationship/{id}")
	public ResponseEntity<?> updateFriendship(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<String, Object>();
		Relationship currentRelationship;
		try {
			currentRelationship = relationshipService.findRelationshipById(id);
		} catch (NoSuchElementException e) {
			response.put("message", "The relationship with id '".concat(id.toString().concat("' not exists.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		relationshipService.updateRelationship(currentRelationship);
		response.put("message", "friendship request successfully accepted");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@DeleteMapping("/relationship/{id}")
	public ResponseEntity<Map<String, Object>> deleteRelationship(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			relationshipService.deleteRelationshipById(id);
		} catch (IllegalArgumentException e) {
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		response.put("message", "Deleted relationship / friendship request correctly");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

}