package es.eoi.redsocial.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.eoi.redsocial.dto.MessageDTO;
import es.eoi.redsocial.dto.ReactionDTO;
import es.eoi.redsocial.dto.RelationshipDTO;
import es.eoi.redsocial.entity.Message;
import es.eoi.redsocial.entity.Reaction;
import es.eoi.redsocial.entity.User;
import es.eoi.redsocial.service.MessageService;
import es.eoi.redsocial.service.ReactionService;
import es.eoi.redsocial.service.RelationshipService;
import es.eoi.redsocial.service.UserService;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/messages")

public class MessagesController {

	@Autowired
	private MessageService messageService;

	@Autowired
	private UserService userService;

	@Autowired
	private ReactionService reactionService;

	@Autowired
	private RelationshipService relationshipService;

	private final ModelMapper modelMapper = new ModelMapper();

	@GetMapping("/{id}")
	public ResponseEntity<MessageDTO> showMessageById(@PathVariable Long id) {

		Message message = messageService.findByIdMessage(id);

		MessageDTO messageDTO = modelMapper.map(message, MessageDTO.class);

		return ResponseEntity.ok(messageDTO);

	}

	@GetMapping("/user/{id}")
	public ResponseEntity<List<MessageDTO>> showMessageByUserId(@PathVariable Long id) {
		User user = userService.finByIdUser(id);
		List<Message> messages = user.getMessage();
		List<MessageDTO> listmessage = new ArrayList<MessageDTO>();
		for (Message message : messages) {
			listmessage.add(modelMapper.map(message, MessageDTO.class));
		}
		return ResponseEntity.ok(listmessage);
	}

	@PostMapping
	public ResponseEntity<Map<String, Object>> createMessage(@RequestBody MessageDTO messageDto,
			@RequestParam Long userId) {
		Map<String, Object> response = new HashMap<String, Object>();
		User user;
		Message message = null;
		try {
			user = userService.finByIdUser(userId);

		} catch (DataAccessException e) {
			response.put("message", "Error acessing to database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		message = modelMapper.map(messageDto, Message.class);
		message.setUser(user);
		Message messageCreate = messageService.saveMessage(message);
		response.put("messageDto", modelMapper.map(messageCreate, MessageDTO.class));

		return ResponseEntity.ok(response);

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Map<String, Object>> deleteMessage(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<String, Object>();

		try {
			messageService.deleteMessageById(id);
		} catch (IllegalArgumentException e) {

			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	@GetMapping("/user/{id}/friendPost")
	public ResponseEntity<?> showFriendMessages(@PathVariable Long id, @RequestParam Long idFriend) {
		List<MessageDTO> friendPostsList = new ArrayList<MessageDTO>();
		Map<String, Object> response = new HashMap<String, Object>();

		java.lang.reflect.Type type = new TypeToken<List<MessageDTO>>() {
		}.getType();

		java.lang.reflect.Type typeRelationship = new TypeToken<List<RelationshipDTO>>() {
		}.getType();

		try {

			List<RelationshipDTO> allUserRelationship = modelMapper.map(relationshipService.getAllRelationships(),
					typeRelationship);

			for (RelationshipDTO relationship : allUserRelationship) {

				if (relationship.getUserFollowing().getId().equals(id)
						&& relationship.getUserFollow().getId().equals(idFriend)
						|| relationship.getUserFollowing().getId().equals(idFriend)
								&& relationship.getUserFollow().getId().equals(id)) {
					friendPostsList = modelMapper.map(userService.finByIdUser(idFriend).getMessage(), type);
				} else {
					response.put("Error", "These users are not friends");
					return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
				}
			}

		} catch (IllegalArgumentException e) {
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(friendPostsList, HttpStatus.OK);
	}

	@GetMapping("/{id}/reactions")
	public ResponseEntity<Map<String, Object>> showReactionByMessageId(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<String, Object>();
		Message message = null;
		try {
			message = messageService.findByIdMessage(id);
		} catch (NoSuchElementException e) {
			response.put("message", "Event with ID ".concat(id.toString().concat(" don't exist.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		List<Reaction> reactions = message.getReaction();
		List<ReactionDTO> listreaction = new ArrayList<ReactionDTO>();
		for (Reaction reaction : reactions) {
			listreaction.add(modelMapper.map(reaction, ReactionDTO.class));
		}
		response.put("listreaction", listreaction);

		return ResponseEntity.ok(response);
	}

	@PostMapping("/{messageId}/reactions")
	public ResponseEntity<Map<String, Object>> createReaction(@RequestBody ReactionDTO reactionDto,
			@RequestParam Long userId, @PathVariable Long messageId) {
		Map<String, Object> response = new HashMap<String, Object>();
		User user = null;
		Message message = null;
		Reaction reaction = null;
		try {
			user = userService.finByIdUser(userId);
			message = messageService.findByIdMessage(messageId);
		} catch (NoSuchElementException e) {
			response.put("message", "User with ID ".concat(userId.toString()
					.concat(" or message with ID ".concat(messageId.toString().concat(" don't exist.")))));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		reaction = modelMapper.map(reactionDto, Reaction.class);
		reaction.setUser(user);
		reaction.setMessage(message);
		reactionService.newReaction(reaction);
		response.put("reactionDto", modelMapper.map(reaction, ReactionDTO.class));
		return ResponseEntity.ok(response);
	}
	
	@GetMapping
	public ResponseEntity<?> showAllMessages() {
		Map<String, Object> response = new HashMap<String, Object>();
		List<Message> messages = new ArrayList<Message>();
		List<MessageDTO> messageAllDTOs = new ArrayList<MessageDTO>();
		try {
			messages = messageService.findAllMessages();
		} catch (DataAccessException exception) {
			response.put("message", "Error al acceder a la base de datos");
			response.put("error",
					exception.getMessage().concat(": ").concat(exception.getMostSpecificCause().getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		if (messages.isEmpty()) {
			response.put("mensaje", "No hay ningun mensaje");
			return ResponseEntity.ok(response);
		}
		for (Message message : messages) {
			messageAllDTOs.add(modelMapper.map(message, MessageDTO.class));
		}
		return ResponseEntity.ok(messageAllDTOs);
	}


}