package es.eoi.redsocial.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.eoi.redsocial.dto.AssistanceDTO;
import es.eoi.redsocial.dto.EventDTO;
import es.eoi.redsocial.dto.UserDTO;
import es.eoi.redsocial.entity.Assistance;
import es.eoi.redsocial.entity.Event;
import es.eoi.redsocial.entity.User;
import es.eoi.redsocial.enums.State;
import es.eoi.redsocial.service.AssistanceService;
import es.eoi.redsocial.service.EventService;
import es.eoi.redsocial.service.UserService;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/events")
public class EventsController {

	@Autowired
	EventService eventService;
	@Autowired
	AssistanceService assistanceService;
	@Autowired
	UserService userService;

	private final ModelMapper modelMapper = new ModelMapper();
	private Map<String, Object> response = new HashMap<String, Object>();

	@GetMapping
	public ResponseEntity<?> findAllEvents() {

		List<EventDTO> eventDTO = new ArrayList<EventDTO>();

		java.lang.reflect.Type type = new TypeToken<List<EventDTO>>() {
		}.getType();
		try {
			eventDTO = modelMapper.map(eventService.findAll(), type);
		} catch (DataAccessException e) {
			response.put("message", "Error acessing to database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(eventDTO, HttpStatus.OK);
	}

	@GetMapping("/{eventId}")
	public ResponseEntity<?> findEvent(@PathVariable Long eventId) {
		Event event;

		try {
			event = eventService.findById(eventId);
		} catch (NoSuchElementException e) {
			response.put("message", "Event with ID ".concat(eventId.toString().concat(" don't exist.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		} catch (DataAccessException e) {
			response.put("message", "Error finding data");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		EventDTO eventDTO = modelMapper.map(event, EventDTO.class);
		response.put("Event", eventDTO);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

	}

	@PostMapping
	public ResponseEntity<?> createEvent(@RequestBody EventDTO eventDTO) {
		Event newEvent = modelMapper.map(eventDTO, Event.class);

		try {
			eventService.save(newEvent);
		} catch (DataAccessException e) {
			response.put("message", "Error inserting/acessing in data base");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("newEvent", modelMapper.map(newEvent, EventDTO.class));

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	// Si ya hay una asistencia negativa, se convierte a positiva, si no, se crea
	// una nueva
	@PostMapping("/{eventId}/yesAssistance")
	public ResponseEntity<?> assistEvent(@PathVariable Long eventId, @RequestParam("userId") Long userId) {
		Assistance assistance;

		try {
				assistance = new Assistance();
				assistance.setEvent(eventService.findById(eventId));
				assistance.setUser(userService.finByIdUser(userId));
				assistance.setState(State.ATTEND);
				assistanceService.save(assistance);
			
		} catch (DataAccessException e) {
			response.put("message", "Error finding assistance or inserting in data base");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (IllegalArgumentException e) {
			response.put("message", "Assistance is already created");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
		}

		response.put("newAssistance", modelMapper.map(assistance, AssistanceDTO.class));

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	// La asistencia positiva se convierte a negativa
	@PostMapping("/{eventId}/notAssistance")
	public ResponseEntity<?> desistEvent(@RequestParam Long assistanceId) {
		Assistance assistance = modelMapper.map(assistanceService.findById(assistanceId), Assistance.class);

		try {
			if (assistance.getState() == State.ATTEND) {
				// El usuario esta asistiendo y quiere cambiar el estado
				assistance.setState(State.NOTATTEND);
				assistanceService.save(assistance);
			} else {
				// El usuario ya no esta asistiendo al evento
				response.put("message", "User already is not attending to this event");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
			}
		} catch (DataAccessException e) {
			response.put("message", "Error finding assistance or modifying in data base");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (IllegalArgumentException e) {
			response.put("message", "You're already not assisting to this event");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
		}

		response.put("updatedAssistance", modelMapper.map(assistance, AssistanceDTO.class));

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@GetMapping("/{eventId}/users/yesAssistance")
	public ResponseEntity<?> findAllAssistingUsers(@PathVariable Long eventId) {
		Event event = eventService.findById(eventId);
		List<User> users = userService.findAllUser();
		List<Assistance> assistances = assistanceService.findAll();

		List<UserDTO> usersDTO = new ArrayList<>();

		try {

			for (Assistance assistance : assistances) {
				if (assistance.getEvent().equals(event)) {
					for (User user : users) {
						if (assistance.getUser().equals(user) && assistance.getState().equals(State.ATTEND)) {
							usersDTO.add(modelMapper.map(user, UserDTO.class));
						}
					}
				}
			}

		} catch (DataAccessException e) {
			response.put("message", "Error finding data");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NoSuchElementException e) {
			response.put("message", "Event with ID ".concat(eventId.toString().concat(" don't exist.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(usersDTO, HttpStatus.OK);
	}

	@GetMapping("/{eventId}/users/notAssistance")
	public ResponseEntity<?> findAllDesistingUsers(@PathVariable Long eventId) {
		Event event = eventService.findById(eventId);
		List<User> users = userService.findAllUser();
		List<Assistance> assistances = assistanceService.findAll();

		List<UserDTO> usersDTO = new ArrayList<>();

		try {

			for (Assistance assistance : assistances) {
				if (assistance.getEvent().equals(event)) {
					for (User user : users) {
						if (assistance.getUser().equals(user) && assistance.getState().equals(State.NOTATTEND)) {
							usersDTO.add(modelMapper.map(user, UserDTO.class));
						}
					}
				}
			}

		} catch (DataAccessException e) {
			response.put("message", "Error finding data");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NoSuchElementException e) {
			response.put("message", "Event with ID ".concat(eventId.toString().concat(" don't exist.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(usersDTO, HttpStatus.OK);
	}

	@GetMapping("/user/{userId}/yesAssistance")
	public ResponseEntity<?> findAllEventsUserAssist(@PathVariable Long userId) {
		User user = userService.finByIdUser(userId);
		List<Assistance> assistances = assistanceService.findAll();

		List<EventDTO> eventDTO = new ArrayList<>();

		try {

			for (Assistance assistance : assistances) {
				if (assistance.getUser().equals(user) && assistance.getState().equals(State.ATTEND)) {
					eventDTO.add(modelMapper.map(assistance.getEvent(), EventDTO.class));
				}
			}

		} catch (DataAccessException e) {
			response.put("message", "Error finding data");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NoSuchElementException e) {
			response.put("message", "User with ID ".concat(userId.toString().concat(" don't exist.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(eventDTO, HttpStatus.OK);
	}

	@GetMapping("/user/{userId}/notAssistance")
	public ResponseEntity<?> findAllEventsUserDesist(@PathVariable Long userId) {
		User user = userService.finByIdUser(userId);
		List<Assistance> assistances = assistanceService.findAll();

		List<EventDTO> eventDTO = new ArrayList<>();

		try {

			for (Assistance assistance : assistances) {
				if (assistance.getUser().equals(user) && assistance.getState().equals(State.NOTATTEND)) {
					eventDTO.add(modelMapper.map(assistance.getEvent(), EventDTO.class));
				}
			}

		} catch (DataAccessException e) {
			response.put("message", "Error finding data");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NoSuchElementException e) {
			response.put("message", "User with ID ".concat(userId.toString().concat(" don't exist.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(eventDTO, HttpStatus.OK);
	}
}
