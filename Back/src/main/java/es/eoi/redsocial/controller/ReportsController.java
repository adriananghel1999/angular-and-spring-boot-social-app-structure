package es.eoi.redsocial.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.eoi.redsocial.dto.EventDTO;
import es.eoi.redsocial.dto.UserDTO;
import es.eoi.redsocial.entity.Event;
import es.eoi.redsocial.entity.User;
import es.eoi.redsocial.service.AssistanceService;
import es.eoi.redsocial.service.EventService;
import es.eoi.redsocial.service.UserService;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/reports")
public class ReportsController {

	@Autowired
	EventService eventService;
	@Autowired
	AssistanceService assistanceService;
	@Autowired
	UserService userService;

	private final ModelMapper modelMapper = new ModelMapper();
	private Map<String, Object> response = new HashMap<String, Object>();

	@GetMapping("/bestEvents")
	public ResponseEntity<?> bestEvents() {
		List<EventDTO> topThreeEvents = new ArrayList<EventDTO>();
		List<Event> allEvents = eventService.findAll();

		java.lang.reflect.Type type = new TypeToken<List<EventDTO>>() {
		}.getType();

		try {

			Comparator<Event> cmp = new Comparator<Event>() {
				@Override
				public int compare(Event o1, Event o2) {
					return Double.compare(o2.getAssistance().size(), o1.getAssistance().size());
				}
			};

			allEvents.sort(cmp);
			topThreeEvents = modelMapper.map(allEvents, type);
			topThreeEvents.subList(3, topThreeEvents.size()).clear();

		} catch (DataAccessException e) {
			response.put("message", "Error acessing to database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(topThreeEvents, HttpStatus.OK);
	}

	// Peor Asistencia
	@GetMapping("/worstEvents")
	public ResponseEntity<?> worstEvents() {
		List<EventDTO> topThreeEvents = new ArrayList<EventDTO>();
		List<Event> allEvents = eventService.findAll();

		java.lang.reflect.Type type = new TypeToken<List<EventDTO>>() {
		}.getType();

		try {

			Comparator<Event> cmp = new Comparator<Event>() {
				@Override
				public int compare(Event o1, Event o2) {
					return Double.compare(o1.getAssistance().size(), o2.getAssistance().size());
				}
			};

			allEvents.sort(cmp);
			topThreeEvents = modelMapper.map(allEvents, type);
			topThreeEvents.subList(3, topThreeEvents.size()).clear();

		} catch (DataAccessException e) {
			response.put("message", "Error acessing to database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

		}

		return new ResponseEntity<>(topThreeEvents, HttpStatus.OK);
	}

	@GetMapping("/passedEvents")
	public ResponseEntity<?> findPassedEvents() {
		List<EventDTO> passedEvents = new ArrayList<>();
		List<Event> events = eventService.findAll();

		Date calendario = new Date();
		
		try {

		for (Event event : events) {
			if (event.getEventDate().before(calendario)) {
				passedEvents.add(modelMapper.map(event, EventDTO.class));

			}

		}
		
		} catch (DataAccessException e) {
			response.put("message", "Error acessing to database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

		}

		return new ResponseEntity<>(passedEvents, HttpStatus.OK);
	}

	@GetMapping("/inProgressEvents")
	public ResponseEntity<?> inProgressEvents() {
		List<EventDTO> inProgressEvents = new ArrayList<EventDTO>();
		List<Event> events = eventService.findAll();

		Date calendario = new Date();
		
		try {

		for (Event event : events) {
			if (event.getEventDate().after(calendario)) {
				inProgressEvents.add(modelMapper.map(event, EventDTO.class));

			}

		}
		
		} catch (DataAccessException e) {
			response.put("message", "Error acessing to database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

		}

		return new ResponseEntity<>(inProgressEvents, HttpStatus.OK);
	}

	@GetMapping("/bestActiveUsers")
	public ResponseEntity<?> bestActiveUsers() {
		List<UserDTO> bestActiveUsers = new ArrayList<UserDTO>();
		List<User> allUsers = userService.findAllUser();

		java.lang.reflect.Type type = new TypeToken<List<EventDTO>>() {
		}.getType();

		try {

			Comparator<User> cmp = new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return Double.compare(o2.getEvent().size(), o1.getEvent().size());
				}
			};

			allUsers.sort(cmp);
			bestActiveUsers = modelMapper.map(allUsers, type);
			bestActiveUsers.subList(3, bestActiveUsers.size()).clear();

		} catch (DataAccessException e) {
			response.put("message", "Error acessing to database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(bestActiveUsers, HttpStatus.OK);
	}

	@GetMapping("/bestWritterUsers")
	public ResponseEntity<?> bestWritterUsers() {
		List<UserDTO> bestWritterUsers = new ArrayList<UserDTO>();
		List<User> allUsers = userService.findAllUser();

		java.lang.reflect.Type type = new TypeToken<List<EventDTO>>() {
		}.getType();

		try {

			Comparator<User> cmp = new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return Double.compare(o2.getMessage().size(), o1.getMessage().size());
				}
			};

			allUsers.sort(cmp);
			bestWritterUsers = modelMapper.map(allUsers, type);
			bestWritterUsers.subList(3, bestWritterUsers.size()).clear();

		} catch (DataAccessException e) {
			response.put("message", "Error acessing to database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(bestWritterUsers, HttpStatus.OK);
	}

	@GetMapping("/bestFriendlyUsers")
	public ResponseEntity<?> bestFriendlyUsers() {
		List<UserDTO> bestFriendlyUsers = new ArrayList<UserDTO>();
		List<User> allUsers = userService.findAllUser();

		java.lang.reflect.Type type = new TypeToken<List<EventDTO>>() {
		}.getType();

		try {

			Comparator<User> cmp = new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return Double.compare(o2.getRelationship1().size(), o1.getRelationship1().size());
				}
			};

			allUsers.sort(cmp);
			bestFriendlyUsers = modelMapper.map(allUsers, type);
			bestFriendlyUsers.subList(3, bestFriendlyUsers.size()).clear();

		} catch (DataAccessException e) {
			response.put("message", "Error acessing to database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(bestFriendlyUsers, HttpStatus.OK);
	}

	@GetMapping("/bestAssistanceUsers")
	public ResponseEntity<?> bestAssistanceUsers() {
		List<UserDTO> bestAssistanceUsers = new ArrayList<UserDTO>();
		List<User> allUsers = userService.findAllUser();

		java.lang.reflect.Type type = new TypeToken<List<EventDTO>>() {
		}.getType();

		try {

			Comparator<User> cmp = new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return Double.compare(o2.getEvent().size(), o1.getEvent().size());
				}
			};

			allUsers.sort(cmp);
			bestAssistanceUsers = modelMapper.map(allUsers, type);
			bestAssistanceUsers.subList(3, bestAssistanceUsers.size()).clear();

		} catch (DataAccessException e) {
			response.put("message", "Error acessing to database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(bestAssistanceUsers, HttpStatus.OK);
	}

}
