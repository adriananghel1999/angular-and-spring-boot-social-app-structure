package es.eoi.redsocial.service;

import java.util.List;

import es.eoi.redsocial.entity.Event;

public interface EventServiceImpl {

	List<Event> findAll();

	void save(Event event);

	Event findById(Long id);

}
