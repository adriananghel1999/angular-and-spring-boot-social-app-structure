package es.eoi.redsocial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.eoi.redsocial.entity.Event;
import es.eoi.redsocial.repository.EventRepository;

@Service
public class EventService implements EventServiceImpl {

	@Autowired
	EventRepository eventRepository;

	@Override
	public List<Event> findAll() {
		return eventRepository.findAll();
	}

	@Override
	public void save(Event event) {
		eventRepository.save(event);
	}

	@Override
	public Event findById(Long id) {
		return eventRepository.findById(id).get();
	}

}
