package es.eoi.redsocial.service;

import java.util.List;

import es.eoi.redsocial.entity.Message;


public interface MessageServiceImpl {

	 Message findByIdMessage(Long id);
	
	 Message saveMessage(Message message);
	 
	 void deleteMessageById(Long id);

	List<Message> findAllMessages();
	
}
