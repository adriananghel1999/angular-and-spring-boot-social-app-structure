package es.eoi.redsocial.service;

import java.util.List;

import es.eoi.redsocial.entity.User;

public interface UserServiceImpl {
	
	
	List<User> findAllUser();

	User saveUser(User user);

	User finByIdUser(Long id);
	
	User findUserAndPass(String user, String pass);
	
	User findUserUsers(String user);


}
