package es.eoi.redsocial.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.eoi.redsocial.entity.User;
import es.eoi.redsocial.repository.UserRepository;

@Service
public class UserService implements UserServiceImpl {

	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional
	public User saveUser(User user) {
		return userRepository.save(user);
	}

	@Override
	@Transactional(readOnly = true)
	public User finByIdUser(Long id) {
		User user = null;
		if (userRepository.findById(id).isPresent() == true) {
			user = userRepository.findById(id).get();
		}
		return user;

	}

	@Override
	public User findUserAndPass(String user, String pass) {
		User userIdPass = userRepository.findUserAndPass(user, pass);
		return userIdPass;
	}

	@Override
	public List<User> findAllUser() {
		return userRepository.findAll();
	}

	@Override
	public User findUserUsers(String user) {
		return userRepository.findUserUsers(user);
	}

}