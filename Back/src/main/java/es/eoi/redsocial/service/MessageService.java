package es.eoi.redsocial.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.eoi.redsocial.entity.Message;
import es.eoi.redsocial.repository.MessageRepository;

@Service
public class MessageService implements MessageServiceImpl {

	@Autowired
	private MessageRepository messageRepository;

	@Override
	@Transactional(readOnly = true)
	public Message findByIdMessage(Long id) {

		return messageRepository.findById(id).get();
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Message> findAllMessages() {
		return messageRepository.findAll();
	}


	@Override
	@Transactional
	public Message saveMessage(Message message) {
		return messageRepository.save(message);
	}

	@Override
	@Transactional
	public void deleteMessageById(Long id) {
		messageRepository.deleteById(id);

	}

}
