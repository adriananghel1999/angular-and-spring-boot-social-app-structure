package es.eoi.redsocial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.eoi.redsocial.entity.Assistance;
import es.eoi.redsocial.repository.AssistanceRepository;

@Service
public class AssistanceService implements AssistanceServiceImpl {

	@Autowired
	AssistanceRepository assistanceRepository;

	@Override
	public List<Assistance> findAll() {
		return assistanceRepository.findAll();
	}

	@Override
	public Assistance save(Assistance assistance) {
		return assistanceRepository.save(assistance);
	}

	@Override
	public Assistance findById(Long id) {
		return assistanceRepository.findById(id).get();
	}

}
