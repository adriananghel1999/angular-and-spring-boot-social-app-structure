package es.eoi.redsocial.service;

import es.eoi.redsocial.entity.Reaction;

public interface ReactionServiceImpl {
	
	Reaction newReaction(Reaction reaction);
}
