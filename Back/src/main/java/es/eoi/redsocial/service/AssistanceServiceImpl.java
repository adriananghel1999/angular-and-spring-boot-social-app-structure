package es.eoi.redsocial.service;

import java.util.List;

import es.eoi.redsocial.entity.Assistance;

public interface AssistanceServiceImpl {

	List<Assistance> findAll();

	Assistance save(Assistance assistance);

	Assistance findById(Long id);

}
