package es.eoi.redsocial.service;

import java.util.List;

import es.eoi.redsocial.entity.Relationship;
import es.eoi.redsocial.entity.User;

public interface RelationshipServiceImpl {
	
	
	Relationship saveRelationship(User user1, User user2);

	Relationship findRelationshipById(Long id);

	List<Relationship> getAllRelationships();

	Relationship updateRelationship(Relationship relationship);

	void deleteRelationshipById(Long id);

}
