package es.eoi.redsocial.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.eoi.redsocial.entity.Relationship;
import es.eoi.redsocial.entity.User;
import es.eoi.redsocial.enums.RelationshipEnum;
import es.eoi.redsocial.repository.RelationshipRepository;

@Service
public class RelationshipService implements RelationshipServiceImpl {

	@Autowired
	private RelationshipRepository repository;

	@Override
	@Transactional
	public Relationship findRelationshipById(Long id) {
		return repository.findById(id).get();
	}

	@Override
	@Transactional
	public void deleteRelationshipById(Long id) {
		repository.deleteById(id);
	}

	@Override
	@Transactional
	public Relationship saveRelationship(User user1, User user2) {

		Relationship Relationship = new Relationship();

		List<Relationship> AllRelationshipsUser1 = new ArrayList<Relationship>();

		user1.getRelationship1().stream().forEach(r -> AllRelationshipsUser1.add(r));
		user1.getRelationship2().stream().forEach(r -> AllRelationshipsUser1.add(r));

		if (AllRelationshipsUser1.stream()
				.anyMatch(r -> r.getUserFollow().equals(user2) || r.getUserFollow().equals(user1)) == true) {
			throw new IllegalArgumentException("These users already included a friendship relationship");
		} else {
			Relationship.setUserFollowing(user1);
			Relationship.setUserFollow(user2);
			Relationship.setState(RelationshipEnum.PENDING);
			repository.save(Relationship);
		}

		return Relationship;
	}

	@Override
	@Transactional
	public List<Relationship> getAllRelationships() {
		return repository.findAll();
	}

	@Override
	@Transactional
	public Relationship updateRelationship(Relationship relationship) {
		relationship.setState(RelationshipEnum.FRIEND);
		return repository.save(relationship);

	}

}