package es.eoi.redsocial.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.eoi.redsocial.entity.Reaction;
import es.eoi.redsocial.repository.ReactionRepository;

@Service
public class ReactionService implements ReactionServiceImpl{

	@Autowired
	private ReactionRepository reactionRepository;

	@Override
	@Transactional
	public Reaction newReaction(Reaction reaction) {
		
		return reactionRepository.save(reaction);
	}
}
