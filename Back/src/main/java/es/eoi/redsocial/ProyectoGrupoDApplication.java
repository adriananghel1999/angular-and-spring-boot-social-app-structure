package es.eoi.redsocial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoGrupoDApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoGrupoDApplication.class, args);
	}

}