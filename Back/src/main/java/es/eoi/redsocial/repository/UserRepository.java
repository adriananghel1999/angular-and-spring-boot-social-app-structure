package es.eoi.redsocial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import es.eoi.redsocial.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query("SELECT u FROM User u WHERE u.user = ?1 AND u.pass = ?2")
	User findUserAndPass(String user, String pass);

	@Query("SELECT u FROM User u WHERE u.user = ?1")
	User findUserUsers(String user);

}