package es.eoi.redsocial.enums;

public enum ReactionType {
	LIKE(0,"LIKE"), HATE(1,"HATE"), LOVE(2,"LOVE");

	private final int claveReaction;
	private final String tipo;
	
	private ReactionType(int claveReaction, String tipo) {
		this.claveReaction = claveReaction;
		this.tipo = tipo;
	}

	public int getClaveReaction() {
		return claveReaction;
	}

	public String getTipo() {
		return tipo;
	}

}