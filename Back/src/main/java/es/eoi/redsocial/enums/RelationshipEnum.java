package es.eoi.redsocial.enums;

public enum RelationshipEnum {

	FRIEND(1, "FRIEND"), PENDING(2, "PENDING");

	private final int code;
	private final String type;

	private RelationshipEnum(int code, String type) {
		this.code = code;
		this.type = type;
	}

	public int getcode() {
		return code;
	}

	public String gettype() {
		return type;
	}

}